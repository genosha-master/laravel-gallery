<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGalleriesMultimediaListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('galleries_multimedia_lists', function (Blueprint $table) {
            $table->increments('gallery_multimedia_list_id');
            $table->integer('multimedia_file_id')->unsigned()->nullable();//fk null
            $table->integer('gallery_id')->unsigned()->nullable();//fk null
            $table->foreign('multimedia_file_id')->references('multimedia_file_id')->on('multimedia_files');
            $table->foreign('gallery_id')->references('gallery_id')->on('galleries');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('galleries_multimedia_lists');
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImgsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('multimedia_files', function (Blueprint $table) { // si o si le paso img, titulo, owner
            $table->increments('multimedia_file_id');
            $table->integer('multimedia_file_owner')->unsigned();//fk
            $table->string('multimedia_file_title');
            $table->string('multimedia_file_description')->nullable();
            //$table->boolean('img_approved')->nullable();
            $table->string('file');//este seria el path de antes 
            $table->integer('multimedia_file_category_id')->unsigned()->nullable();//fk null
            $table->foreign('multimedia_file_category_id')->references('multimedia_file_category_id')->on('categories_imgs');
            $table->foreign('multimedia_file_owner')->references('id')->on('users');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('multimedia_files');
    }
}

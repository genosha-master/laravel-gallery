<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Multimedia_File extends Model
{
    protected $table = 'multimedia_files';
    protected $primaryKey = 'multimedia_file_id';
    protected $fillable = ['multimedia_file_title', 'multimedia_file_description', 'multimedia_file_video'];
}




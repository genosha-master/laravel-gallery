<?php
View::addExtension('html', 'php');

// HOME PAGE ===================================
Route::get('/', function() {
    return response()->view('angular');
});

// API ROUTES ==================================
Route::group(array('prefix' => 'api'), function() {

});

// PROJECTS ===================================
Route::get('/projects','ProjectRestController@index');
Route::get('/projects/{id}','ProjectRestController@show');
//Route::get('/projects/category/{$id}','ImgRestController@showByCategory'); // Filtra por categoria


// IMAGES ===================================
Route::get('/images','ImgRestController@index'); // Me devuelve todo
Route::get('/images/{id}','ImgRestController@show'); //Me devuelve 1 sola


// DRAWINGS ===================================
//Route::get('/drawings','DrawingRestController@index'); // Me devuelve todo

// MAIL ===================================
Route::resource('mail','MailController');

Route::get('/mailto', function () {
        return view('contacto');
    });

// LOGIN ===================================
Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

// CATCH ALL ROUTE =============================
Route::any('{all}', function($exception) {
    return response()->view('angular');
})->where('all', '.*');


<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallery_Multimedia_List extends Model
{
    protected $table = 'galleries';
    protected $primaryKey = 'gallery_id';
    protected $fillable = ['gallery_name'];
}
